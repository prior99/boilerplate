import { createStore, applyMiddleware, compose, Store, StoreEnhancer } from "redux";

import rootReducer, { AppState } from "./reducers";
import loggerMiddleware from "./middlewares/logger";
import thunkMiddleware from "./middlewares/thunk";

/**
 * Setup up the initial Store to be ready to use for the rest of the application.
 * This function will apply all middlewares into a single combined store enhancer and compose it with
 * the redux devtools store enhancer. Both will then be together with the root reducer used to create
 * the store.
 */
export default function configureStore(): Store<AppState> {
    /*
     * Setup chrome redux devtools extension if installed in the current browser (and in browser at all)
     * For more information please see: https://github.com/zalmoxisus/redux-devtools-extension
     */
    const anyWindow: any = window as any;
    const devTools: StoreEnhancer<AppState> =
        anyWindow && anyWindow.__REDUX_DEVTOOLS_EXTENSION__ && anyWindow.__REDUX_DEVTOOLS_EXTENSION__();

    /*
     * Setup all middlewares. If you want to insert a new middleware into the chain of middlewares,
     * this would be the right place to do so. Simply add it to the list of arguments in the following call,
     * Paying attention to the order you are using.
     */
    const middlewares: StoreEnhancer<AppState> = applyMiddleware(
        thunkMiddleware,
        loggerMiddleware
    );

    /*
     * Compose middlewares and devTools to one function
     */
    const composedMiddlewares = compose(middlewares, devTools);
    return createStore<AppState>(rootReducer, devTools ? composedMiddlewares : middlewares);
}
