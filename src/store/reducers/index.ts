import { combineReducers } from "redux";
import loginReducer, { LoginState } from "./login";

export interface AppState {
    readonly login: LoginState;
}

/**
 * Combines all reducers into the root reducer which will be used to create and update the applications whole
 * state.
 */
export default combineReducers<AppState>({
    // Here is the right place to mount new reducers
    login: loginReducer
});
