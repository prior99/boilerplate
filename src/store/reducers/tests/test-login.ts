import loginReducer, { ValidationStatus } from "../login";
import { LoginActions } from "../../../actions/login";
import { assert } from "chai";

describe("The login Reducer", () => {
    const oldState = {
        username: "Cptn.",
        password: "1234",
        validationStatus: ValidationStatus.INVALID
    };

    describe("with the Login/UPDATE_USERNAME action", () => {
        const action = {
            type: LoginActions.UPDATE_USERNAME,
            payload: "Cptn. Obvious"
        };

        it("sets the username in an empty state with the actions payload", () => {
            const newState = loginReducer(undefined, action);
            const expectedState = {
                username: "Cptn. Obvious",
                password: "",
                validationStatus: ValidationStatus.PENDING
            };
            assert.deepEqual(newState, expectedState);
        });

        it("updates the username in an existing state with the actions payload", () => {
            const newState = loginReducer(oldState, action);
            const expectedState = {
                username: "Cptn. Obvious",
                password: "1234",
                validationStatus: ValidationStatus.INVALID
            };
            assert.deepEqual(newState, expectedState);
        });
    });
    describe("with the Login/UPDATE_PASSWORD action", () => {
        const action = {
            type: LoginActions.UPDATE_PASSWORD,
            payload: "hallo123"
        };

        it("updates the password in an existing state with the actions payload", () => {
            const newState = loginReducer(oldState, action);
            const expectedState = {
                username: "Cptn.",
                password: "hallo123",
                validationStatus: ValidationStatus.INVALID
            };
            assert.deepEqual(newState, expectedState);
        });
    });
    describe("with an unknown action", () => {
        const action = {
            type: "UNKNOWN_ACTION"
        };

        it("updates the password in an existing state with the actions payload", () => {
            const newState = loginReducer(oldState, action);
            assert.strictEqual(newState, oldState);
        });
    });
});
