import { Action } from "../../actions";
import { isUpdatePassword, isUpdateUsername } from "../../actions/login";

export enum ValidationStatus {
    VALID,
    PENDING,
    IN_PROGRESS,
    INVALID
}

export interface LoginState {
    readonly username: string;
    readonly password: string;
    readonly validationStatus: ValidationStatus;
}

const defaultState: LoginState = {
    username: "",
    password: "",
    validationStatus: ValidationStatus.PENDING
};
/**
 * This reducer generates the state for the login component. It will store the username as well as the password
 * when the respective actions were dispatched.
 */
export default function loginReducer(state: LoginState = defaultState, action: Action<any>) {
    if (isUpdateUsername(action)) {
        return { ...state, username: action.payload };
    }
    if (isUpdatePassword(action)) {
        return { ...state, password: action.payload };
    }
    return state;
}
