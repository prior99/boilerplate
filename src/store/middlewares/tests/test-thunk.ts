import { assert } from "chai";
import ThunkMiddleware from "../thunk";

describe("The thunk middleware", () => {
    const store = null;

    describe("with an asynchroneous action", () => {
        async function createMockAction(): Promise<any> {
            const someData = await Math.min(1, 2, 3, 4);
            return {
                type: "DO_SOMETHING_USEFULL",
                payload: someData
            };
        }

        it("resolves the payload Promise and hands the result over to the next middleware", (done) => {
            function dispatch(action) {
                const expected = {
                    type: "DO_SOMETHING_USEFULL",
                    payload: 1
                };
                assert.deepEqual(action, expected);
                done();
            }

            ThunkMiddleware(store)(dispatch)(createMockAction());
        });
    });

    describe("with a non-asynchroneous action", () => {
        const mockAction = {
            type: "EAT_DONUTS",
            payload: 2
        };
        it("simply hands over the action to the next middleware", (done) => {
            function dispatch(action) {
                const expected = {
                    type: "EAT_DONUTS",
                    payload: 2
                };
                assert.deepEqual(action, expected);
                done();
            }

            ThunkMiddleware(store)(dispatch)(mockAction);
        });
    });
});
