import { Dispatch, Store } from "redux";
import { Action } from "../../actions";
import { AppState } from "../reducers";

/* tslint:disable:no-console */
/**
 * This middleware logs all actions whenever an action was dispatched. It logs a group to the console which contains:
 *  - The name of the action
 *  - The state before dispatching the action
 *  - The state after dispatching the action
 *  - A trace
 * It does not modify any action in any way and just hands it on to the next middleware.
 */
const loggerMiddleware = (store: Store<AppState>) => (next: Dispatch<AppState>) => (action: Action<any>) => {
    console.groupCollapsed(`Dispatching  ${action.type}`);
    console.log("prev state", store.getState());
    next(action);
    console.log("next state", store.getState());
    console.trace();
    console.groupEnd();
};
/* tslint:enable */

export default loggerMiddleware;
