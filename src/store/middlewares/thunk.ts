import { Dispatch } from "redux";
import { Action } from "../../actions";
import { AppState } from "../reducers";

type ThunkInputAction = Promise<Action<any>> | Action<any>;

/**
 * Checks if an action is actually an asynchronous action (a promise).
 *
 * @param suspectedPromise The action which will be checked.
 */
function isPromise(suspectedPromise: ThunkInputAction): suspectedPromise is Promise<Action<any>> {
    // TODO: Maybe a more sophisticated check would be appropriate.
    if (!suspectedPromise) {
        return false;
    }
    // Check if the suspected promise contains a function called "then" and if it does, treat it as a Promise.
    const suspectedPromiseType = typeof (suspectedPromise as Promise<Action<any>>).then;
    return suspectedPromiseType === "function";
}

/**
 * This middleware will check if a dispatched action is and asynchronous one and the resolve it before
 * handing it to the next middleware. This way it is possible to dispatch actions which will make a request
 * to the API or do something else in the background and this middleware will take care of resolving them.
 */
const thunkMiddleware = () => (next: Dispatch<AppState>) => async (action: ThunkInputAction): Promise<void> => {
    if (isPromise(action)) {
        // Resolve the action and hand the result to the next middleware.
        try {
            const resolvedAction = await action;
            next(resolvedAction);
        } catch (err) {
            // Log every error which might occur while resolving the action, then re-throw it.
            /* tslint:disable no-console */
            console.error("An error occured when resolving an asynchroneous action in the Thunk middleware:", err);
            console.trace();
            /* tslint:enable */
            throw err;
        }
    }
    // Just pass-through actions which are not asynchronous.
    next(action as Action<any>);
};

export default thunkMiddleware;
