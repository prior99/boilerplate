import * as React from "react";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import Login from "../login";

interface AppProps {

}

/**
 * Top-level component containing the whole applications virtual DOM.
 */
export default class App extends React.Component<AppProps, undefined> {
    public render() {
        return (
            <MuiThemeProvider>
                <Login />
            </MuiThemeProvider>
        );
    }
}
