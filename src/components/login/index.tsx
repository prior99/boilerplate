import * as React from "react";
import { Dispatch } from "redux";
import { connect } from "react-redux";
import TextField from "material-ui/TextField";
import RaisedButton from "material-ui/RaisedButton";
import { AppState } from "../../store/reducers";
import {
    updateUsername,
    updatePassword,
    login
} from "../../actions/login";
import { ValidationStatus } from "../../store/reducers/login";

interface LoginHandlerProps {
    onUsernameChange: (event: React.SyntheticEvent<HTMLInputElement>) => void;
    onPasswordChange: (event: React.SyntheticEvent<HTMLInputElement>) => void;
    onLoginClick: () => void;
}

interface LoginStateProps {
    username: string;
    password: string;
    validationStatus: ValidationStatus;
}

interface LoginOwnProps {

}

type LoginProps = LoginStateProps & LoginHandlerProps & LoginOwnProps;

function mapDispatchToProps(dispatch: Dispatch<AppState>): LoginHandlerProps {
    return {
        onUsernameChange: (event: React.SyntheticEvent<HTMLInputElement>) => {
            dispatch(updateUsername((event.target as HTMLInputElement).value));
        },
        onPasswordChange: (event: React.SyntheticEvent<HTMLInputElement>) => {
            dispatch(updatePassword((event.target as HTMLInputElement).value));
        },
        onLoginClick: () => {
            dispatch(login());
        }
    };
}

function mapStateToProps(state: AppState): LoginStateProps {
    const { login } = state;
    const { username, password, validationStatus } = login;
    return {
        username,
        password,
        validationStatus
    };
}

/**
 * This component is responsible for rendering the Login Page.
 */
class Login extends React.Component<LoginProps, void> {
    public render() {
        const { onUsernameChange, onPasswordChange, onLoginClick, username, password, validationStatus } = this.props;
        return(
            <form>
                <p>
                    <TextField hintText="Username" onChange={onUsernameChange} value={username} />
                </p>
                <p>
                    <TextField type="password" hintText="Password" onChange={onPasswordChange} value={password} />
                </p>
                <RaisedButton primary={true} onClick={onLoginClick}>Login</RaisedButton>
                <div>{ValidationStatus[validationStatus]}</div>
            </form>
        );
    }
}

const ConnectedLogin: React.ComponentClass<LoginOwnProps> =
    connect<LoginStateProps, LoginHandlerProps, LoginProps>(mapStateToProps, mapDispatchToProps)(Login);

export default ConnectedLogin;
