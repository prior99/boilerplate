import * as React from "react";
import * as ReactDom from "react-dom";
import { Provider } from "react-redux";

import App from "./components/app";
import configureStore from "./store/configure-store";

/*
 * Needed polyfills for older browsers
 */
import * as PromisePolyfill from "es6-promise";
PromisePolyfill.polyfill();

/*
 * DOM id of the container the whole application will be rendered into
 */
const containerId = "app";

const store = configureStore();

/*
 * If element with specified id does not exist in DOM, create a new empty div
 * into which the application can then be rendered.
 */
if (!document.getElementById(containerId)) {
    const div = document.createElement("div");
    div.id = containerId;
    document.body.appendChild(div);
}

/*
 * After having ensured a container exists to render the app into, the react
 * application can now be rendered initially.
 */
ReactDom.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById(containerId)
);
