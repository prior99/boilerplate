import { Action } from ".";

export const LoginActions = {
    PERFORM: "LOGIN/PERFORM",
    UPDATE_USERNAME: "LOGIN/UPDATE_USERNAME",
    UPDATE_PASSWORD: "LOGIN/UPDATE_PASSWORD"
};

/*
 * Perform Login
 */
export type ActionLogin = Action<void>;
/**
 * Creates an action for performing the actual login.
 */
export function login(): ActionLogin {
    return {
        type: LoginActions.PERFORM
    };
}

export function isLogin(action: Action<any>): action is ActionLogin {
    return action.type === LoginActions.PERFORM;
}

/*
 * Update Username
 */
export type ActionUpdateUsername = Action<string>;
/**
 * Creates an action for updating the username in the applications state.
 * @param username The username to store in the state.
 */
export function updateUsername(username: string): ActionUpdateUsername {
    return {
        type: LoginActions.UPDATE_USERNAME,
        payload: username
    };
}

export function isUpdateUsername(action: Action<any>): action is ActionUpdateUsername {
    return action.type === LoginActions.UPDATE_USERNAME;
}

/*
 * Update Password
 */
export type ActionUpdatePassword = Action<string>;
/**
 * Creates an action for updating the password in the applications state.
 * @param password The password to store in the state.
 */
export function updatePassword(password: string): ActionUpdatePassword {
    return {
        type: LoginActions.UPDATE_PASSWORD,
        payload: password
    };
}
export function isUpdatePassword(action: Action<any>): action is ActionUpdatePassword {
    return action.type === LoginActions.UPDATE_PASSWORD;
}
