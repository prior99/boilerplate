const ProductionConfig = require("./webpack.config");
const merge = require("ramda").merge;

module.exports = merge(ProductionConfig, {
    devtool: "inline-source-map",
    module: {
        loaders: [
            {
                test: /\.tsx?$/,
                loaders: ["istanbul-instrumenter-loader", "awesome-typescript-loader"]
            }, 
            {
                test: /\.s?css$/,
                loader: "ignore-loader"
            }
        ]
    }
});
