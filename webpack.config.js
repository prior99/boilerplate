const Webpack = require("webpack");
const Autoprefixer = require("autoprefixer");
const Precss = require("precss");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractCSS = new ExtractTextPlugin('[name].css');

module.exports = {
    entry: "./src/index.tsx",
    output: {
        filename: "bundle.js",
        path: __dirname + "/dist",
        publicPath: "/dist"
    },
    devtool: "source-map",
    resolve: {
        extensions: [".ts", ".tsx", ".json", ".js", ".jsx"]
    },
    devServer: {
        inline: true,
        port: 35400
    },
    plugins: [
        extractCSS
    ],
    module: {
        rules: [
            {
                test: /\.(png|svg)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            hash: "sha512",
                            digest: "hex",
                            name: "[hash].[ext]"
                        }
                    },
                    {
                        loader: "image-loader-webpack",
                        options: {
                            bypassOnDebug: true,
                            OptimizationLevel: 7,
                            interlaced: false
                        }
                    }
                ]
            },
            {
                test: /\.s?css$/,
                loader: extractCSS.extract([
                    {
                        loader: "css-loader",
                        query: {
                            modules: true,
                            importLoaders: 1,
                            localIdentName: '[path]_[name]_[hash:base64:5]',
                            sourceMap: true
                        }
                    },
                    {
                        loader: "postcss-loader",
                        query: {
                            sourceMap: true
                        }
                    }
                ])
            },
            {
                test: /\.tsx?$/,
                use: [
                    {
                        loader: "awesome-typescript-loader"
                    }
                ]
            },
            {
                test: /\.js$/,
                use: [
                    {
                        loader: "source-map-loader"
                    }
                ]
            }
        ]
    }
};
