# Boilerplate

A modern boilerplate meant for easy starting of scalable, stable and quality web applications.

## Usage

### Starting

Install the neccessary depdendencies using npm as usual, start the dev server and jump right in.
The dev server will automaticall build the sources for you and host the webpage on ```http://localhost:35400/```.
Full sourcemap-support is available in both the browser and the tests.
The [Redux Devtools Extension](https://github.com/zalmoxisus/redux-devtools-extension) is also supported.


```sh
user@machine:~/project/$ npm install
user@machine:~/project/$ npm start
```

Then connect to http://localhost:35400/.

### Testing

#### Code coverage and testing

You can run all the tests and get a coverage result by simply running the following:

```sh
user@machine:~/project/$ npm test
```

Multiple browsers are supported. Just set your ```BROWSER``` environment variable accordingly:

 - Firefox
 - Chrome
 - PhantomJS (For headless testing)

In order to test in one or more of them,
just set the ```BROWSER``` environment variable to  comma-seperated list like this:

```sh
user@machine:~/project/$ BROWSER=Chrome,Firefox npm test
```

#### Linting

In order to lint your code with tslint run:

```sh
user@machine:~/project/$ npm run lint:src
```

You can also lint your styles with stylelint like so:

```sh
user@machine:~/project/$ npm run lint:style
```

## About

### Technologies

This boilerplate is using the following technologies:

 - [Typescript](https://www.npmjs.com/package/typescript) as language dialect.
 - [React](https://www.npmjs.com/package/react) as primary frontend framework.
 - [Redux](https://www.npmjs.com/package/redux) as model management for the application.
 - [Mocha](https://www.npmjs.com/package/mocha) for testing.
 - [Karma](https://www.npmjs.com/package/karma) for frontend testing.
 - [Webpack](https://www.npmjs.com/package/webpack) as module bundle.
 - [Less](https://www.npmjs.com/package/less) for styling.

### Tools

 - [TSLint](https://www.npmjs.com/package/tslint) to ensure consistent and managable code.
 - [Karma](https://www.npmjs.com/package/karma) for frontend testing.
 - [Mocha](https://www.npmjs.com/package/mocha) for testing.
 - [GitLab CI](https://about.gitlab.com/gitlab-ci/) for testing on the CI.

### Features

The boilerplate can out of the box:

 - Render a simple application using react
 - Have styling with Less using CSS modules.
 - Call external REST apis.

### Contributors

The following people contributed to this project:

 - Frederick Gnodtke

### License

This boilerplate is licensed under the terms of the 3-Clause BSD License. For more information see the LICENSE file.
