const webpack = require("webpack");
const webpackConfig = require("./webpack.config-test");

const browser = (process.env["BROWSER"] && process.env["BROWSER"].toLowerCase()) || ""; 
const browsers = browser.split(",").reduce((list, current) => {
    if (current === "chrome") {
        return [...list, "Chrome"];
    } else if (current === "firefox") {
        return [...list, "Firefox"];
    } else if (current === "phantom") {
        return [...list, "PhantomJS"];
    } else {
        return list;
    }
}, []);

if (!browsers.length) {
    browsers.push("PhantomJS");
}

module.exports = (config) => {
    config.set({
        browsers,
        singleRun: true,
        frameworks: ["mocha"],
        plugins: [
            "karma-phantomjs-launcher",
            "karma-chai",
            "karma-mocha",
            "karma-sourcemap-loader",
            "karma-webpack",
            "karma-coverage",
            "karma-mocha-reporter",
            "karma-firefox-launcher",
            "karma-chrome-launcher"
        ],
        files: [
            "src/**/tests/test-*.ts",
            "src/**/tests/test-*.tsx"
        ],
        preprocessors: {
            "src/**/tests/test-*.ts": ["webpack", "sourcemap"],
            "src/**/tests/test-*.tsx": ["webpack", "sourcemap"]
        },
        reporters: [
            "mocha",
            "coverage"
        ],
        coverageReporter: {
            dir: "coverage/",
            reporters: [
                { type: "text" }
            ]
        },
        webpack: webpackConfig,
        webpackMiddleware: {
            noInfo: true,
            stats: false
        },
        colors: true,
    });
};
